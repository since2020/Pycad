﻿# Pycad
Python Extension for AutoCad

Editor/Debuger



## 简要说明:

Pycad是利用ironpython来实现对CAD.net API的利用,使得大家得以在CAD上面使用Python语言进行编程.

我们致力于适配上AutoDeskCAD/浩辰CAD/中望CAD等主流CAD.



## 使用方法： 

1. 程序支持AutoCad2013-2020（2014版本以下需安装.Net4.5），将压缩文件下载后解压到任意目录，在AutoCad在用Netload命令选择解压目录中的NFox.Pycad.Acad.dll，同时支持浩辰Cad，对应加载文件为NFox.Pycad.Gcad.dll。

2. 程序选用vscode作为编辑器，在Cad中键入pye命令，可以自动打开vscode，当然你需要安装vscode，以及vscode的Python扩展（微软）和Pythonv3.7+。

3. 如果代码修改完成，在Cad中键入pyrb命令可以即时编译python的脚本。

4. pytest项目有很多例程可以参考。

5. vscode右下角有一个即时窗口，可以直接在这里键入或者从代码区拷贝代码直接执行看结果

6. 修改py项目的组织形式,原项目组织形式为包，不能防止同名包覆盖的问题,新的项目组织形式与国际接轨, 根目录可随意命名, 保证目录下有个extension包即可, 该包不可重命名, 不可缺失；项目的data目录可放置数据文件，程序中可使用findfile(filename)获取文件路径；项目的cuix目录可放置Cad的cuix文件用于加载定制的菜单、工具条和Ribbon菜单；重写了发布功能, 命令pyrelease, 可选择多项目一起发布, 完成后会在temp目录下生成一个自解压安装包xxx.Setup.exe；可以在未安装Pycad的机器上安装Pycad的运行版本和开发的项目；pye命令名修改为pyedit。

7. 调试器基本完成,提供测试，调试流程: 打开Cad->打开pytest项目->按下F5->在Cad中敲命令即可进入调试模式；退出调试只需要点vscode中的断开连接按钮。

8. 修正发布功能，增加invokeArx模块以调用Arx函数，例子见pytest项目的runtime模块

感谢山人编写的文档，但是改版暂时只能借鉴了，我们一起把他顶出来写新版的吧：）

如果pye命令出现“系统找不到指定的文件”的错误，请在“Pycad\bin”目录下找到配置文件settings.json，修改“editor.path”参数为vscode的安装目录.

细节问题：

1、关于vscode 自动提示配置说明：
在vscode配置信息修改这两个地方

![输入图片说明](https://images.gitee.com/uploads/images/2020/1213/184746_cb67de73_5553512.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1213/184916_ef4bfb5f_5553512.png "屏幕截图.png")



## 使用教程： 

### 基本用法

1、启动命令
* 打开cad，在命令行输入netload，如果是autocad，请加载 NFox.Pycad.Acad.dll 如果是浩辰cad，请加载 NFox.Pycad.Gcad.dll，
* 在命令行输入pyedit，系统会提示你打开项目或者创建项目等选项，选择完毕后，程序自动打开vscode，如果没有安装vscode,请自行安装vscode。
* 编写完命令后，输入pyrb进行项目编译。
 
2、导入模块文件：
   
```python
from pycad.runtime import *
from pycad.system import *
```  

3、开始第一个程序：hello world
       
```python
@command()
def test(doc)
    print("hello world")
```
* 第一行 @command():代表下面定义的函数是个命令函数。命令名称是test，
  doc是传入参数，是当前主文档对象，如果不太理解的话。可以用下面方法调试查看

```python
@command()
def test2(doc):   
    print "the type is :" , type(doc)
    print "the dwg name is :" , doc.Name
    args=dir(doc)
    for val in args:
         print "the doc object has method or propertyname is :", val
```

### Autocad对象
pycad主要对objectarx.net的二次封装，在system与runtime中，封装了下面的对象：

```python
"acap": "Autodesk.AutoCAD.ApplicationServices",
"acdb": "Autodesk.AutoCAD.DatabaseServices",
"aced": "Autodesk.AutoCAD.EditorInput",
"acge": "Autodesk.AutoCAD.Geometry",
"acrx": "Autodesk.AutoCAD.Runtime",
"acws": "Autodesk.AutoCAD.Windows",
"acgi": "Autodesk.AutoCAD.GraphicsInterface",
"acgs": "Autodesk.AutoCAD.GraphicsSystem",
"acin": "Autodesk.AutoCAD.Internal",
"acps": "Autodesk.AutoCAD.PlottingServices",
"acco": "Autodesk.AutoCAD.Colors",
```
编写方式跟c#二次开发差不多



### 图形的创建方法：

```python
@command()
def enttest(doc):
    with  dbtrans(doc) as tr:
        btr=tr.opencurrspace()  #这里默认只读打开，.netapi推荐对象打开先以读打开以节省资源，再需要写入时提权

        #创建点
        point=acdb.DBPoint(acge.Point3d(2,2,0))
        tr.addentity(btr,point)  #addentity会将btr提权为写打开，并将对象加入数据库

        #创建直线 第一个参数：起点  第二个参数：终点
        line=acdb.Line(acge.Point3d(2,2,0),acge.Point3d(4,5,1)) 
        tr.addentity(btr,line)

        #创建圆形 第一个参数:起点 第二个参数:投影向量，第三个参数:半径
        c1=acdb.Circle(acge.Point3d(10,10,0),acge.Vector3d(0,0,1),1)  # z轴投影
        c2=acdb.Circle(acge.Point3d(20,20,0),acge.Vector3d(0,1,0),2)  # y轴投影
        c3=acdb.Circle(acge.Point3d(30,30,0),acge.Vector3d(1,0,0),3)  # x轴投影
        tr.addentity(btr,c1,c2,c3) #or: tr.addentity(btr, *(c1,c2,c3))


        #创建多段线
        pl=acdb.Polyline()
        pl.AddVertexAt(0,acge.Point2d(0,0),0,0,0) #添加顶点
        pl.AddVertexAt(1,acge.Point2d(0,1),0,0,0)
        pl.AddVertexAt(2,acge.Point2d(1,1),0,0,0)
        pl.AddVertexAt(3,acge.Point2d(1,0),0,0,0)
        pl.Closed=True                            #是否闭合
        tr.addentity(btr,pl)

        #创建弧线 第一个参数：弧的圆心 第二个参数：半径 第三个参数： 起点角度 第四个参数：终点角度
        arc=acdb.Arc(acge.Point3d(10,0,0),3,math.pi*0.5,math.pi*1.25)
        tr.addentity(btr,arc)

```

### 图层的创建方法：
#### 默认创建图层：
```python

    @command()
    def createlayer(doc):
        LayerName="MyLayer"
        with dbtrans(doc) as tr:
            tr.addlayer(LayerName)
        print("\n创建{0}图层成功".format(LayerName))

```
#### 添加带颜色的图层
```python

@command()
def cl1(doc):
    LayerName="MyLayer2"
    with dbtrans(doc) as tr:
        tr.addlayer(LayerName,acco.Color().FromColorIndex(acco.ColorMethod.ByLayer,2))               
    print("\n创建{0}图层成功".format(LayerName))

```


#### 修改图层属性
```python

@command() #修改图层属性
def ModifyLayer(doc): 
    res=edx.entsel("\n选择要修改的图形：")
    if not res.ok(): return
    with dbtrans(doc) as tr:
        ent=tr.getobject(res.ObjectId)  
        ltr=tr.getobject(res.LayerId,acdb.OpenMode.ForWrite) #获取LayerTableRecord表记录
        ltr.IsFrozen=True   #冻结图层
        ltr.IsLocked=True   #锁定图层
        print("\n{0}图层已经冻结和锁定".format(ent2.Layer))

```

## 错误类型： 
### xxx() takes no arguments (1 given)
```python
    @command()
    def cl1(): #这里缺少参数doc，正确写法为 def cl1(doc)
```

## 作者信息:

本工程第一作者为:**xsfhlzh**

